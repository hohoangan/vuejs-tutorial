var vm = new Vue({
    el:'#app', 
    data:{
        title: 'Dien thoai Samsung',
        url:'https://translate.google.com/',
        target: '_blank',
        price: 10900
    } ,
    methods:{
        formatPrice(){
            return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(this.price);
        }
    }
  })
  console.log(vm);